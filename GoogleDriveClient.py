from googleapiclient import discovery
from Common import GoogleUtilities

class GoogleDriveClient(GoogleUtilities):
    def __init__(self,config):
        self.api_info = config['api']
        self.file_info = config['files']
        self.program_info = config['program']
        GoogleUtilities.__init__(self,self.api_info['name'],
                                 self.api_info['scope'],
                                 self.file_info['client_secret'],
                                 self.file_info['credentials'])
        self.client = discovery.build('drive',credentials=self.credentials,version='v2')
    def list_files(self,keyword):
        self.refresh()
        result = self.client.files().list(maxResults = 10).execute()
        items = result.get('files',[])
        print "files: "
        for i in items:
            print "{0} ID:{1}".format(i["name"],i["id"])
        pass