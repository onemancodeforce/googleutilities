from googleapiclient import discovery
from googleapiclient.http import MediaIoBaseDownload,MediaIoBaseUpload
import gdata.photos.service
import gdata.media
import gdata.geo
from datetime import datetime
import os
import atom
import httplib2
import json

from oauth2client.file import Storage
import oauth2client.client as Client
import io


api_config = {
    "name":"Crawler", #api name
    "email":"twhregal02@gmail.com",
    "type":"all"
}

apiLinks={
    "drive":{
        "link":["https://www.googleapis.com/auth/drive"],
        "credentials":"./keys/drive_credentials.json"
             },
    "all":{
        "link":["https://picasaweb.google.com/data/","https://www.googleapis.com/auth/drive"],
        "credentials":"./keys/credentials.json"
    },
    "photos":{
        "link":["https://picasaweb.google.com/data/"],
        "credentials":"./keys/photos_credentials.json"
    },
    "client_secret":"./keys/client_secret.json"
}

class GoogleDriveFolder:
    def create(self):
        pass

    def search(self):
        pass

    def move(self):
        pass

    def delete(self):
        pass

    def rename(self):
        pass

class GoogleAuthorizer(object):
    def __init__(self,
                 app_name,
                 type
                 ):
        temp = apiLinks.get(type,'empty')
        if(temp=='empty'):
            print("Error: Type of API does not exist")
            raise IOError
        self.scope = temp.get('link')

        self.token = None
        self.additional_headers = None
        self.application_name = app_name
        self.credentials = self.getCredentials(apiLinks['client_secret'],temp['credentials'])

    def getCredentials(self,client_secret_path,credentials_path):
        store = Storage(credentials_path)
        credentials= store.get()
        if not credentials or credentials.invalid:
            flow = Client.flow_from_clientsecrets(client_secret_path,
                                                  self.scope,
                                                  redirect_uri = 'urn:ietf:wg:oauth:2.0:oob')
            flow.user_agent = self.application_name
            auth_uri = flow.step1_get_authorize_url()
            print(auth_uri)
            auth_code = raw_input('Enter the authentication code: ')
            credentials = flow.step2_exchange(auth_code)
            store.put(credentials)
        credentials.authorize(httplib2.Http())
        return credentials

    def refreshToken(self):
        self.credentials.refresh(httplib2.Http())
        self.access_token = self.credentials.access_token

    def authorize(self):
        return self.credentials.authorize(httplib2.Http())

    def refresh(self):
        self.credentials.refresh(httplib2.Http())
        self.access_token = self.credentials.access_token
        self.additional_headers = {'Authorization':'Bearer '+ self.access_token}

    def get_header(self):
        return self.additional_headers

class GoogleActions:
    def __init__(self):
        self.authorizer = None
        self.client = None
        pass
    def refresh_client(self):
        self.authorizer.refresh()
        self.client.additional_header = self.authorizer.get_header()

class GoogleDriveAtomicDataType:
    def __init__(self):
        self.client=None
        pass
    def atomicSearch(self,query,all=False):
        items = []
        next_page = None
        while(True):
            result = self.client.files().list(q=query, pageToken=next_page).execute()
            items += result.get('files', [])
            next_page = result.get('nextPageToken')
            if(not(all and not next_page)):
                break
        # append with default []

        # add next_page_token
        print ("files: ")
        # for i in items:
        #     print ("{0} ID:{1}".format(i["name"].encode('UTF-8'), i["id"]))
        return items
    def atomicDownload(self,fn,fileID,metadata=None):

        request = self.client.files().get_media(fileId=fileID)
        fh = io.FileIO(fn,'wb')
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
            print "Download %d%%." % int(status.progress() * 100)
        print(request)
        # request = client.files().export_media(fileID=fileID)
        # with open(fn,'wb') as f:
        #     f.write(fh)

class GoogleDriveFile(GoogleActions,GoogleDriveAtomicDataType):
    def __init__(self,clienta,authorizera):
        GoogleDriveAtomicDataType.__init__(self)
        GoogleActions.__init__(self)
        self.client = clienta
        self.authorizer = authorizera
        pass
    def search(self,keyword,exact=False,all=False):
        self.refresh_client()
        query = ""
        if(exact):
            query = "(name = '" + keyword + "')"
        else:
            query = "(name contains '" + keyword + "')"
        return self.atomicSearch(query,all)

    def download(self,keyword,folder=".",type=None,all=False):
        self.refresh_client()
        items = self.search(keyword,type,True,all)
        for i in items:
            print(i)
            self.atomicDownload(self.client,
                                      (folder+"/"+i.get('name')),
                                      i.get('id'))
    def download_all(self,keyword,folder=".",type=None,all=False):
        self.refresh_client()
        items = self.search(keyword,type,False,all)
        for i in items:
            print(i)
            self.atomicDownload(self.client,
                                      (folder+"/"+i.get('name')),
                                      i.get('id'))
    # def download_to_dir(self,keyword,dir,type=None,all=False):


class GoogleDriveClient:

    def __init__(self, fp):

        config = None
        with open(fp, 'r') as f:
            config = json.load(f)
        self.authorizer = GoogleAuthorizer(api_config['name'],api_config['type'])
        self.client = discovery.build('drive', 'v3', http=self.authorizer.authorize())
        self.client.email = api_config['email']
        self.client.additional_header = self.authorizer.get_header()
        self.file = GoogleDriveFile(self.client,self.authorizer)
        # self.folder = GoogleDriveFolder(self.client,self.authorizer)













    def search_page_by_name(self,keyword,next_page = None):
        self.refresh_client()
        items = []
        query= "(name contains '"+keyword+"')"
        count = 0
        if not (next_page):
            result = self.client.files().list(q=query).execute()
        #returns a dict/json
        else:
            result = self.client.files().list(q=query,pageToken=next_page).execute()
            #next page
        #append with default []
        items += result.get('files',[])

        #add next_page_token
        next_page = result.get('nextPageToken')
        print ("files: ")
        for i in items:
            print ("{0} ID:{1}".format(i["name"].encode('UTF-8'),i["id"]))
        return items, next_page
    def search_file(self,keyword,all=False):
        items = []
        nextPage=None
        while (True):
            temp,nextPageTemp = self.search_page_by_name(keyword,nextPage)
            nextPage = nextPageTemp
            items +=temp
            if(nextPage is None or not all):
                break
        return items
    def get_file_dict(self):
        items = []
        nextPage = None
        while(True):
            temp,temp2 = self.search_folder("",nextPage)
            items+=temp
            nextPage=temp2
            if(nextPage is None):
                break
        return items
    def search_folder(self,keyword,next_page=None):
        self.refresh_client()
        items = []
        query = "mimeType='application/vnd.google-apps.folder'"
        count = 0
        result = self.client.files().list(q=query,pageToken=next_page).execute()
        # returns a dict/json

        items += result.get('files', [])

        # add next_page_token
        next_page = result.get('nextPageToken')
        print ("files: ")
        for i in items:
            print ("{0} ID:{1}".format(i["name"].encode('UTF-8'), i["id"]))
        return items, next_page

class GooglePhotosClient:
    def __init__(self, fp):
        config = None
        with open(fp,'r') as f:
            config = json.load(f)
        self.api_info = config['api']
        self.file_info = config['files']
        self.program_info = config['program']
        # GoogleUtilities.__init__(self,self.api_info['name'],
        #                          "https://picasaweb.google.com/data/",
        #                          self.file_info['client_secret'],
        #                          self.file_info['credentials'])
        self.client = gdata.photos.service.PhotosService()
        self.client.email =self.api_info['email']
        self.authorize()
        self.refresh()
        self.albums = None
    def get_albums(self):
        albums = self.client.GetUserFeed()
        self.albums = albums.entry
    def make_album(self,album_name,comment):
        #deprecated
        photo= self.client.InsertAlbum(title= album_name,
                                       summary=comment)
    def InsertPhoto(self,fp,album_id,photo_name,comment):
        album_url = '/data/feed/api/user/%s/albumid/%s'%(self.client.email,
                                                         album_id
                                                         )
        # now_time = datetime.now()
        # entry = gdata.photos.PhotoEntry(author = self.client,
        #                                 timestamp=now_time,
        #                                 title = photo_name,
        #                                 text = comment
        #                                 )
        # photo = self.client.InsertPhoto(album_url,
        #                                 entry,
        #                                 fp,
        #                                 content_type = "image/jpeg")
        photo = self.client.InsertPhotoSimple(
            album_url,photo_name,
            comment,fp,
            content_type="image/jpeg"
        )
    def getAlbumId(self,title):
        for i in self.albums:
            if(i.title.text ==title):
                return i.gphoto_id.text

    def batchUpload(self,
                    photo_dir,
                    album_id,
                    upload_msg):
        abs_path = os.path.abspath(photo_dir)
        for i in os.listdir(photo_dir):
            try:
                self.InsertPhoto(os.path.join(photo_dir,i),album_id,i+".jpg",upload_msg)
                print "uploaded: "+i
            except Exception, Error:
                print Error
                print "link: "+ i
                pass

    def batchUploadAndDelete(self,
                             photo_dir,
                             album_name,upload_msg):
        album_id = self.getAlbumId(album_name)
        self.batchUpload(photo_dir,
                         album_id,
                         upload_msg)
        for i in os.listdir(photo_dir):
            os.remove(os.path.join(photo_dir,i))




