from googleapiclient import discovery
from Common import GoogleUtilities
import json
class GoogleDriveClient(GoogleUtilities):
    def __init__(self, fp):
        config = None
        with open(fp, 'r') as f:
            config = json.load(f)
        self.api_info = config['api']
        self.file_info = config['files']
        self.program_info = config['program']
        GoogleUtilities.__init__(self, self.api_info['name'],
                                 ["https://www.googleapis.com/auth/drive"],
                                 self.file_info['client_secret'],
                                 self.file_info['credentials'])
    def list_files(self,keyword):
        self.refresh()
        result = self.client.files().list(maxResults = 10).execute()
        items = result.get('files',[])
        print ("files: ")
        for i in items:
            print ("{0} ID:{1}".format(i["name"],i["id"]))
        pass