
apiLinks={
    "drive":{
        "link":["https://www.googleapis.com/auth/drive"],
        "credentials":"./keys/drive_credentials.json"
             },
    "all":{
        "link":["https://picasaweb.google.com/data/","https://www.googleapis.com/auth/drive"],
        "credentials":"./keys/credentials.json"
    },
    "photos":{
        "link":["https://picasaweb.google.com/data/"],
        "credentials":"./keys/photos_credentials.json"
    },
    "client_secret":"./keys/client_secret.json"
}

class GoogleAuthorizer(object):
    def __init__(self,
                 app_name,
                 type
                 ):
        temp = apiLinks.get(type,'empty')
        if(temp=='empty'):
            print("Error: Type of API does not exist")
            raise IOError
        self.scope = temp.get('link')

        self.token = None
        self.additional_headers = None
        self.application_name = app_name
        self.credentials = self.getCredentials(apiLinks['client_secret'],temp['credentials'])

    def getCredentials(self,client_secret_path,credentials_path):
        store = Storage(credentials_path)
        credentials= store.get()
        if not credentials or credentials.invalid:
            flow = Client.flow_from_clientsecrets(client_secret_path,
                                                  self.scope,
                                                  redirect_uri = 'urn:ietf:wg:oauth:2.0:oob')
            flow.user_agent = self.application_name
            auth_uri = flow.step1_get_authorize_url()
            print(auth_uri)
            auth_code = raw_input('Enter the authentication code: ')
            credentials = flow.step2_exchange(auth_code)
            store.put(credentials)
        credentials.authorize(httplib2.Http())
        return credentials

    def refreshToken(self):
        self.credentials.refresh(httplib2.Http())
        self.access_token = self.credentials.access_token

    def authorize(self):
        return self.credentials.authorize(httplib2.Http())

    def refresh(self):
        self.credentials.refresh(httplib2.Http())
        self.access_token = self.credentials.access_token
        self.additional_headers = {'Authorization':'Bearer '+ self.access_token}

    def get_header(self):
        return self.additional_headers
