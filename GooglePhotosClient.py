import gdata.photos.service
import gdata.media
import gdata.geo
from datetime import datetime
import os
import atom
from Common import GoogleUtilities

class GooglePhotosClient(GoogleUtilities):
    def __init__(self, config):
        self.api_info = config['api']
        self.file_info = config['files']
        self.program_info = config['program']
        GoogleUtilities.__init__(self,self.api_info['name'],
                                 "https://picasaweb.google.com/data/",
                                 self.file_info['client_secret'],
                                 self.file_info['credentials'])
        self.client = gdata.photos.service.PhotosService()
        self.client.email =self.api_info['email']
        self.authorize()
        self.refresh()
        self.albums = None
    def get_albums(self):
        albums = self.client.GetUserFeed()
        self.albums = albums.entry
    def make_album(self,album_name,comment):
        #deprecated
        photo= self.client.InsertAlbum(title= album_name,
                                       summary=comment)
    def InsertPhoto(self,fp,album_id,photo_name,comment):
        album_url = '/data/feed/api/user/%s/albumid/%s'%(self.client.email,
                                                         album_id
                                                         )
        # now_time = datetime.now()
        # entry = gdata.photos.PhotoEntry(author = self.client,
        #                                 timestamp=now_time,
        #                                 title = photo_name,
        #                                 text = comment
        #                                 )
        # photo = self.client.InsertPhoto(album_url,
        #                                 entry,
        #                                 fp,
        #                                 content_type = "image/jpeg")
        photo = self.client.InsertPhotoSimple(
            album_url,photo_name,
            comment,fp,
            content_type="image/jpeg"
        )
    def getAlbumId(self,title):
        for i in self.albums:
            if(i.title.text ==title):
                return i.gphoto_id.text

    def batchUpload(self,
                    photo_dir,
                    album_id,
                    upload_msg):
        abs_path = os.path.abspath(photo_dir)
        for i in os.listdir(photo_dir):
            try:
                self.InsertPhoto(os.path.join(photo_dir,i),album_id,i+".jpg",upload_msg)
                print "uploaded: "+i
            except Exception, Error:
                print Error
                print "link: "+ i
                pass

    def batchUploadAndDelete(self,
                             photo_dir,
                             album_name,upload_msg):
        album_id = self.getAlbumId(album_name)
        self.batchUpload(photo_dir,
                         album_id,
                         upload_msg)
        for i in os.listdir(photo_dir):
            os.remove(os.path.join(photo_dir,i))



